<?php

namespace Drupal\frontend\Form;

use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormCacheInterface;
use Drupal\Core\Form\FormSubmitterInterface;
use Drupal\Core\Form\FormValidatorInterface;

class FrontendFormBuilder implements FormBuilderInterface, FormCacheInterface, FormSubmitterInterface, FormValidatorInterface {
}
