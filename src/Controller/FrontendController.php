<?php

namespace Drupal\frontend\Controller;

use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\RfcLogLevel;


/**
 * Returns responses for DB log UI routes.
 */
class FrontendController extends ControllerBase {
  public function listFiles() {
    $path  = \Drupal::root() . '/frontend';
    $files = scandir($path);

    return [
      '#markup' => 'List:' . print_r($files, true)
    ];
  }
  public function testTwig() {
    return [
      '#theme' => 'frontend',
      '#test'  => '[test variable]'
    ];
  }
  /**
   * Tests template overriding based on filename.
   *
   * @return array
   *   A render array containing a theme override.
   */
  public function testTemplate() {
    return [
      'info' => [
        '#type' => '#markup',
        '#markup' => print_r( \Drupal::service('extension.list.theme_engine')->getList(), true )
      ],
      'vue' => [
        '#theme' => 'theme_test_template_test',
        '#attached' => [
          'drupalSettings' => [
            'vueData' => [
              'message' => 'From server side'
            ]
          ]
        ]
      ]
    ];

    return [
      '#attached' => [
        'drupalSettings' => [
          'vueData' => [
            'message' => 'Server side'
          ]
        ]
      ],
      '#markup' => \Drupal::theme()->render('theme_test_template_test', [
        'engine' => 'nuxt_engine'
      ])
    ];

    /** @var \Drupal\Core\Extension\ThemeEngineExtensionList  */
    /** @var \Drupal\Core\Extension\Extension[]  */
    $engineList = \Drupal::service('extension.list.theme_engine')->getList();
    $themeList = \Drupal::service('extension.list.theme')->getList();

    // if (isset($engineList['nuxt_engine'])) {
    //   $nuxt_engine = $engineList['nuxt_engine'];
    //   $theme = new \Drupal\Core\Extension\Extension(\Drupal::root(), 'theme_engine', $nuxt_engine->getPath(), 'nuxt_engine.engine');
    //   $theme->owner  = $nuxt_engine->getExtensionPathname();
    //   $theme->prefix = $nuxt_engine->getName();
  
    //   $themes = ['disposable_theme' => $theme];
    //   // $themes = \Drupal::moduleHandler()->buildModuleDependencies($themes);
    //   \Drupal::moduleHandler()->buildModuleDependencies([$nuxt_engine]);



    //   $nuxt_loaded = \Drupal::moduleHandler()->load('nuxt_engine');
    // }


    // return [
    //   // '#markup' => print_r($engineList['nuxt_engine'], true)
    //   '#markup' => print_r( \Drupal::service('extension.list.theme_engine')->getExtensionInfo('nuxt_engine') , true)
    //   // '#markup' => print_r( array_keys($themeList), true) . print_r( array_keys($engineList), true) . \nuxt_engine_extension()
    //   // '#markup' => 'loaded: ' . $nuxt_loaded
    // ];

    $result = [
      '#markup' => \Drupal::theme()->render('theme_test_template_test', [
        'engine' => 'nuxt_engine'
      ])
    ];

    return $result;
  }

  // ThemeExtensionList::doList
  protected function doList() {
    // Find themes.
    $themes = parent::doList();

    $engines = $this->engineList->getList();
    // Always get the freshest list of themes (rather than the already cached
    // list in $this->installedThemes) when building the theme listing because a
    // theme could have just been installed or uninstalled.
    $this->installedThemes = $this->configFactory->get('core.extension')->get('theme') ?: [];

    $sub_themes = [];
    // Read info files for each theme.
    foreach ($themes as $name => $theme) {
      // Defaults to 'twig' (see self::defaults above).
      $engine = $theme->info['engine'];
      if (isset($engines[$engine])) {
        $theme->owner = $engines[$engine]->getExtensionPathname();
        $theme->prefix = $engines[$engine]->getName();
      }
      // Add this theme as a sub-theme if it has a base theme.
      if (!empty($theme->info['base theme'])) {
        $sub_themes[] = $name;
      }
      // Add status.
      $theme->status = (int) isset($this->installedThemes[$name]);
    }

    // Build dependencies.
    $themes = $this->moduleHandler->buildModuleDependencies($themes);

    // After establishing the full list of available themes, fill in data for
    // sub-themes.
    $this->fillInSubThemeData($themes, $sub_themes);

    return $themes;
  }

  // checkout core\tests\Drupal\Tests\Core\Theme\RegistryTest::testGetRegistryForModule
  // for theme testing
  public function testVueEngine() {
    $theme_manager = \Drupal::theme();
    return ['#markup' => $theme_manager->render('theme_test_template_test', [])];
    
    $active_theme = $theme_manager->getActiveTheme();
    
    // $temp_theme = clone $active_theme;
    $temp_theme = new \Drupal\Core\Theme\ActiveTheme([
      'name' => $active_theme->getName(),
      'path' => $active_theme->getPath(),
      'engine' => 'vue',
      'owner' => $active_theme->getOwner(),
      'stylesheets_remove' => [],
      'libraries' => $active_theme->getLibraries(),
      'extension' => '.vue',
      'base_theme_extensions' => [],
      'regions' => [],
      'libraries_override' => [],
      'libraries_extend' => [],
    ]);

    \Drupal::logger('engine:', $temp_theme->getEngine());

    $theme_manager->setActiveTheme($temp_theme);

    $result = ['#markup' => \Drupal::theme()->render('theme_test_template_test', [])];

    $theme_manager->setActiveTheme($active_theme);

    return $result;
  }
}
