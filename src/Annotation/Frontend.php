<?php

namespace Drupal\frontend\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the frontend plugin annotation object.
 *
 * Plugin namespace: Plugin\Frontend.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class Frontend extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin label.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
