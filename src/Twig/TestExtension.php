<?php

namespace Drupal\frontend\Twig;

use Drupal\frontend\Twig\TestTwigRuntime;

class TestExtension extends \Twig_Extension {

  /**
   * This string must be unique but there's no special rules on it.
   *
   * The recommended approach is just returning the magic constant __CLASS__
   * since it will always be unique to the class (it contains the fully
   * qualified name of the class which will include the namespace).
   */
  public function getName() {
    return __CLASS__;
  }

  /**
   * This function passes over the definitions of your filter functions
   * to twig. This is done with an array with a format of:
   *
   * new \Twig_SimpleFilter('FUNCTION_NAME_IN_TWIG', FUNCTION_CALLABLE),
   *
   * It's recommended that you include your module name in your Twig filter,
   * function, etc. names to prevent a namespace collision with another module.
   *
   * The function will receive one argument - the variable to the left of the |
   * character in the twig template.
   *
   * For more information, see http://twig.sensiolabs.org/doc/advanced.html#filters.
   */
  public function getFilters() {
    return [
      /**
       * By Closure. This works best with smallish operations.
       */
      new \Twig_SimpleFilter('closure_style', function($var) {
        return 'foo' . strtoupper($var);
      }),

      /**
       * By static method on *any* class, but usually on this class
       */
      new \Twig_SimpleFilter('static_style', [__CLASS__, 'staticMethod']),

      /**
       * Finally to this object, useful if you need to keep some form
       * of internal count of how many times the function was called
       * for some reason.
       */
      new \Twig_SimpleFilter('dynamic_style', [$this, 'objectMethod']),

      new \Twig\TwigFilter('lazy', [\Drupal::service('frontend.twig_runtime'), 'lazyFilter'])
      // new \Twig\TwigFilter('lazy', [TestTwigRuntime::class, 'lazyFilter'])
    ];
  }

  /**
   * Example of a callback for the static method above.
   */
  public static function staticMethod($var) {
    return $var . 'bar';
  }

  /**
   * And object is the same except it doesn't need the static keyword.
   */
  public function objectMethod($var) {
    return 'baa' . $var;
  }

}