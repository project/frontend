<?php

namespace Drupal\frontend\Twig;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Twig\Extension\RuntimeExtensionInterface;

class TestTwigRuntime implements RuntimeExtensionInterface {
  /**
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  public function __construct(LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->logger = $logger_channel_factory->get('frontend:twig');
  }

  public function lazyFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',') {
    $this->logger->info('logger successfully injected');
  
    $price = number_format($number, $decimals, $decPoint, $thousandsSep);
    $price = '$'.$price;

    return $price;
  }
}
