<?php

namespace Drupal\frontend;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

class FrontendServiceProvider extends ServiceProviderBase {
  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Override Twig with Velvet
    $definition = $container->getDefinition('twig');
    $definition->setClass('Drupal\frontend\VelvetAdapter');
  }
}
