<?php

namespace Drupal\frontend\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Core\Generator\GeneratorInterface;

/**
 * Class GenerateCommand.
 *
 * Drupal\Console\Annotations\DrupalCommand (
 *     extension="frontend",
 *     extensionType="module"
 * )
 */
class GenerateCommand extends ContainerAwareCommand {

  /**
   * Drupal\Console\Core\Generator\GeneratorInterface definition.
   *
   * @var \Drupal\Console\Core\Generator\GeneratorInterface
   */
  protected $generator;


  /**
   * Constructs a new GenerateCommand object.
   */
  public function __construct(GeneratorInterface $frontend_frontend_generate_generator) {
    $this->generator = $frontend_frontend_generate_generator;
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('frontend:generate')
      ->setDescription($this->trans('commands.frontend.generate.description'));
  }

  /**
   * {@inheritdoc}
   */
  protected function initialize(InputInterface $input, OutputInterface $output) {
    parent::initialize($input, $output);
    $this->getIo()->info('initialize');
  }

  /**
   * {@inheritdoc}
   */
  protected function interact(InputInterface $input, OutputInterface $output) {
    $this->getIo()->info('interact');
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $this->getIo()->info('execute');
    $this->getIo()->info($this->trans('commands.frontend.generate.messages.success'));
    $this->generator->generate([]);
  }

}
