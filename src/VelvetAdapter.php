<?php

namespace Drupal\frontend;

use Drupal\Core\Template\TwigEnvironment;
// use Velvet\Velvet;

// class VelvetAdapter extends Velvet {
class VelvetAdapter extends TwigEnvironment {
  // public function __construct(LoaderInterface $loader = null, $options = []) {
  //   // parent::__construct($loader, $options);
  // }
  /**
   * {@inheritdoc}
   */
  public function render($name, array $context = []) {
    return 'Rendered from Adapter';
  }
}
