<?php

namespace Drupal\frontend;

use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\RfcLogLevel;


/**
 * Returns responses for DB log UI routes.
 */
class FrontendDetector {
  public function listFiles() {
    $path    = '../frontend';
    $files = scandir($path);

    print_r($files);
  }
}